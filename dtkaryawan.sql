-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 04 Jul 2020 pada 07.44
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dtkaryawan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `biodata`
--

CREATE TABLE `biodata` (
  `nik` varchar(16) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `ttl` varchar(50) NOT NULL,
  `kelamin` varchar(9) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `biodata`
--

INSERT INTO `biodata` (`nik`, `nama`, `ttl`, `kelamin`, `alamat`, `pekerjaan`) VALUES
('1234567887654321', 'Felix Yehezkiel', '17-Maret-2019', 'LAKI-LAKI', 'Jalan jalan', 'Progamer'),
('8765432112345678', 'Felixa', '12-Maret-2019', 'PEREMPUAN', 'Jalan kemana', 'Proproan'),
('8765432112345679', 'Test', 'Jakarta, 17 Maret 1945', 'PEREMPUAN', 'Grand poris', 'Programmer'),
('8765432112345678', 'Kak Ocep', 'Semarang, 17 Agustus 1945', 'LAKI-LAKI', 'Dimana kemana', 'Programmer'),
('1234', 'Test2', 'Jakarta, 17 Maret 1945', 'LAKI-LAKI', 'asd', 'ads'),
('132123', 'Rere', 'Masads', 'LAKI-LAKI', 'asda', 'asdav');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
