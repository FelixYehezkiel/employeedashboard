package com.dtkaryawan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.sql.SQLException;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

public class ServerData extends WebSocketServer {
    public ServerData( int port ) throws UnknownHostException {
        super( new InetSocketAddress( port ) );
    }

    @Override
    public void onOpen( WebSocket conn, ClientHandshake handshake ) {//tempat konek pertama kali
        System.out.println( " ada yang masuk " );
    }

    @Override
    public void onClose( WebSocket conn, int code, String reason, boolean remote ) {//tempat keluar koneksi
        System.out.println( " ada yang keluar " );
    }

    @Override
    public void onMessage( WebSocket conn,   String message ) {//tempat untuk menerima
        Data dataQuery = new Data();
        try {

            dataQuery.query(message);
//            for (String data : dataQuery.getDb()){
//                broadcast(data);
//            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    public static void main( String[] args ) throws InterruptedException , IOException {
        int port = 8887; // 843 flash policy port
        try {
            port = Integer.parseInt( args[ 0 ] );
        } catch ( Exception ex ) {
        }
        ServerData s = new ServerData( port );
        s.start();
        System.out.println( "ServerData started on port: " + s.getPort() );

        BufferedReader sysin = new BufferedReader( new InputStreamReader( System.in ) );
        while ( true ) {
            String in = sysin.readLine();
            s.broadcast( in );
            if( in.equals( "exit" ) ) {
                s.stop(1000);
                break;
            }
        }
    }
    @Override
    public void onError( WebSocket conn, Exception ex ) {
        ex.printStackTrace();
        if( conn != null ) {
            // some errors like port binding failed may not be assignable to a specific websocket
        }
    }

    @Override
    public void onStart() {
        System.out.println("Server started!");
        setConnectionLostTimeout(0);
        setConnectionLostTimeout(100);
    }
}
